from next import *
from flask import Flask,jsonify,request,render_template

app = Flask(__name__)
app.static_folder = 'static'

@app.route('/')
def home_page():
	return render_template("firstspg.html")


@app.route("/get")
def predictor():
	print("success")
	usertext=request.args.get('msg')
	print(usertext)
	return (next_word(usertext))


if __name__ == "__main__":
	app.run(debug=True)